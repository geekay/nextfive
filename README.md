# Next Five

## Description

Returns the next 5 races from an API with a countdown

### API

Steps:

* Install Docker

Run the following command: `docker network create local`

* Add the .env file

Remove the .default tag from the .env.default file

* Install the DB

Navigate to the api/database directory. Once inside run: `make dockerrun`

* Install Goose

[Goose](https://bitbucket.org/liamstask/goose/)

* Goose up

Navigate to the api directory and run `goose up`

* Create data

Run the following command `make testseed races=` with an integer to create the specified number of races.

* Run the API

Run the `make` command to run the API

### Front-end

* NPM Install

Run `npm install` to install node modules

* Run the front-end

Run `make` to set up a live-reloading front-end at [localhost:8080](http://localhost:8080)
