import { applyMiddleware } from 'redux'
import RacesMiddleware from './races/races_middleware'
import { routerMiddleware } from 'react-router-redux'
import { history } from './shared_components/router'

const routeMiddleware = routerMiddleware(history)

const Middleware = applyMiddleware(RacesMiddleware, routeMiddleware)

export default Middleware
