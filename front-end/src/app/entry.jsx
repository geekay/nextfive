import './assets/stylesheets/styles.scss'

// React and Redux
import React from 'react'
import { render } from 'react-dom'

import store from './store'
import { history } from './middleware' 
import Index from './shared_components/router'

document.addEventListener('DOMContentLoaded', () => {
  render(<Index store={ store } history={ history }/>, document.getElementById('index'))
})
