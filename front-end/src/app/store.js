import { createStore } from 'redux'
import reducer from './reducer'
import middleware from './middleware'

const Store = createStore(
    reducer,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
    middleware)

window.store = Store

export default Store
