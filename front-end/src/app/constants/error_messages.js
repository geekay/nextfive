import * as Errors from './error_codes'

const ErrorMessages = {
 [Errors.BcRequestNotExistForCurrentUser]: "You cannot sign until you've entered your Current Lease and Property Manager details",
 [Errors.RentRateNotValid]: 'Rate is not valid',
 [Errors.RentBondNotValid]: 'Bond is not valid',
 [Errors.StartDateRequired]: 'start date is required',
 [Errors.StartDateMustBeInThePast]: 'Start date must be in the past',
 [Errors.EndDateRequired]: 'End date is required for non-rolling lease',
 [Errors.EndDateMustBeAfterStartDate]: 'End date must be after start date',
 [Errors.BcRequestAlreadyExistsForCurrentUser]: 'You already have a BondCover request outstanding',
 [Errors.TenantEmailAddressAlreadyExists]: 'Secondary tenant already exists',
 [Errors.SecondaryTenantNotExist]: 'Only secondary leasees can view this page',
 [Errors.CannotUpdateAlreadyApproved]: 'You cannot update these details after approving',
 [Errors.ErrorManagerCannotBeTenant]: 'A tenant cannot be the manager of a request',
 [Errors.BcRequestNotInProperStateToBeenSigned]: 'BondCover request cannot be signed',
 [Errors.CannotUpdateRequestDetailAfterSigning]: 'The details cannot be updated after BondCover request is signed',
 [Errors.BcRequestCannotBeSubmitted]: 'BondCover request cannot been submitted',
 [Errors.ErrorTenantsBondTotalIncorrect]: 'Bond split total must equal the total bond amount',
 [Errors.ErrorWhenRetrievingCustomer]: "You cannot sign until you've entered your Current Lease and Property Manager details",
 [Errors.ErrorWhenReadingUserID]: "Can't find current user ID",
 [Errors.FirstNameNotValid]: "First Name is not a valid name",
 [Errors.MiddleNameNotValid]: "Middle Name is not a valid name",
 [Errors.LastNameNotValid]: "Middle Name is not a valid name",
 [Errors.InsufficientPrivileges]: "You are not authorised to view this",
}

export default ErrorMessages
