

export const	BcRequestNotExistForCurrentUser        = 200107
export const	RentRateNotValid                       = 200109
export const	RentBondNotValid                       = 200111
export const	StartDateRequired                      = 200112
export const	StartDateMustBeInThePast               = 200113
export const	EndDateRequired                        = 200114
export const	EndDateMustBeAfterStartDate            = 200115
export const  TooCloseToTheEndOfLeaseOrAlreadyEnded  = 200121
export const	BcRequestAlreadyExistsForCurrentUser   = 200125
export const  TenantEmailAddressAlreadyExists        = 200192
export const  SecondaryTenantNotExist                = 200197
export const  CannotUpdateAlreadyApproved            = 200200
export const  ErrorManagerCannotBeTenant             = 200202


export const  BcRequestNotInProperStateToBeenSigned        = 200150
export const 	CannotUpdateRequestDetailAfterSigning        = 200154
export const  BcRequestCannotBeSubmitted                   = 200170
export const  ErrorTenantsBondTotalIncorrect               = 200191
export const  ErrorWhenRetrievingCustomer                  = 300122

export const FirstNameNotValid                       = 700153
export const MiddleNameNotValid                      = 700154
export const LastNameNotValid                        = 700155

export const ErrorWhenReadingUserID                 = 900100
export const InsufficientPrivileges                  = 999998


export const InvalidCredentials = 'auth error'
