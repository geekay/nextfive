import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'

// Reducers
import races from './races/races_reducer'

const appReducer = combineReducers({
  races,
  routerReducer
})

const rootReducer = ( state, action ) => {
  if (action.type === 'RECEIVE_LOGOUT')  {
    state = undefined
  }
  
  return appReducer(state, action)
}

export default rootReducer