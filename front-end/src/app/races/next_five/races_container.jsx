import React from 'react'
import Race from './race-item'

class RacesContainer extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      races: null
    }
    this.reload = this.reload.bind(this)
  }

  componentWillMount() {
    let { fetchNextFive } = this.props;
    fetchNextFive();
  }

  reload() {
    let { fetchNextFive } = this.props;
    fetchNextFive();
  }

  componentWillReceiveProps(nextProps) {
    let { races } = nextProps
    this.setState({ races: races })
  }

  render() {
    let races = this.state.races ? this.state.races : []
    return (
      <div className="races">
        {races.map(i => {
            return <Race
              key={i.raceID}
              raceID={i.raceID}
              raceType={i.raceType}
              closeTime={i.closeTime} 
              reload={this.reload} />
          })}
      </div>
    )
  }
}

export default RacesContainer
