import { connect } from 'react-redux'
import RacesContainer from './races_container'
import { fetchNextFive } from '../races_actions'

const mapStateToProps = ({ races }) => ({
  races: races.nextFive,
})

const mapDispatchToProps = dispatch => ({
  fetchNextFive: () => fetchNextFive()
})

export default connect(mapStateToProps, mapDispatchToProps)(RacesContainer)