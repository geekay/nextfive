import React from 'react'
import { Link } from 'react-router-dom'

class Race extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      timeRemaining: null,
      raceType: null
    }
    this.calculateTimeRemaining = this.calculateTimeRemaining.bind(this)
    this.countdown = this.countdown.bind(this)
  }

  componentDidMount() {
    let { raceType, closeTime } = this.props
    var parsedRaceType
    switch(raceType){
      case 1:
        parsedRaceType = "Thoroughbred"
        break;
      case 2:
        parsedRaceType = "Greyhounds"
        break;
      case 3:
        parsedRaceType = "Harness"
        break;  
    }
    this.setState({raceType: parsedRaceType})
    this.calculateTimeRemaining()

  }

  calculateTimeRemaining() {
    let { closeTime } = this.props
    closeTime = new Date(closeTime).getTime();
    let currentTime = new Date().getTime();

    //Get the difference in seconds
    let difference = Math.ceil((closeTime - currentTime)/1000)
    this.interval = setInterval(this.countdown, 1000);
    this.setState({ timeRemaining: difference })
  }

  componentWillUnmount() {
    clearInterval(this.interval)
  }

  countdown() {
    let { reload } = this.props
    let { timeRemaining } = this.state
    
    timeRemaining = timeRemaining - 1;

    if (timeRemaining == 0) {
      reload()
    }

    this.setState({ timeRemaining: timeRemaining })
  }

  render() {
    let { raceID, closeTime } = this.props
    let { raceType, timeRemaining } = this.state
    return (
      <Link className="race-link" to={`races/${raceID}/competitors`}>
        <div className="race">
          <div className="header">
            Race: { raceType }
          </div>
          <div className="race-id">
            Race ID: { raceID }
          </div>
          <div>
            Time Remaining: { timeRemaining }
          </div>
        </div>
      </Link>
    )
  }
}

export default Race