import $ from 'jquery'

const baseUrl = {
  'development': 'http://localhost:8000'
}[process.env.NODE_ENV]

export const now = Date.now().toString()

export const getNextFive = (success, error) => {
  $.ajax({
    crossDomain: true,
    method: 'get',
    url: `${baseUrl}?cachebuster=${now}`,
    success,
    error
  })
}

export const getCompetitors = (raceID, success, error) => {
  $.ajax({
    crossDomain: true,
    method: 'get',
    url: `${baseUrl}/races/${raceID}/competitors?cachebuster=${now}`,
    success,
    error
  })
}