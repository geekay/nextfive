import merge from 'lodash/merge'

import { LOCATION_CHANGE } from 'react-router-redux'

import {
  RECEIVE_ERROR,
  RECEIVE_RACES,
  RECEIVE_COMPETITORS
} from './races_actions'

const _defaultState = {
  nextFive: null,
  competitors: null,
}

const RacesReducer = ( state = _defaultState, action ) => {
  let {
    type,
    nextFive,
    competitors,
    error
  } = action
  Object.freeze(state)
  let newState = Object.assign({}, state)
  switch(type) {
    case RECEIVE_RACES:
      return Object.assign(newState, {nextFive})
    case RECEIVE_COMPETITORS:
      return Object.assign(newState, {competitors})
    case RECEIVE_ERROR:
      return Object.assign(newState, { error })
    case LOCATION_CHANGE:
      error = ''
      return Object.assign(newState, { error })
    default:
      return state
  }
}

export default RacesReducer
