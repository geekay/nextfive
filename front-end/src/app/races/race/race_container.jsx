import React from 'react'
import Competitor from './competitor'

class RacesContainer extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      competitors: []
    }
  }

  componentWillMount() {
    const { match: { params: { raceid = null } }, fetchCompetitors } = this.props    
    if (raceid != null) {
      fetchCompetitors(raceid);
    }
  }

  componentWillReceiveProps(nextProps) {
    let { competitors } = nextProps
    this.setState({ competitors: competitors })
  }

  render() {
    let { competitors } = this.state
    return (
      <div className="competitors">
        <h1> Competitors </h1>
        <div className="competitor-wrapper">
          {competitors.map(i => {
              return <Competitor key={i.position} name={i.competitorName} position={i.position} />
            })}
        </div>
      </div>
    )
  }
}

export default RacesContainer
