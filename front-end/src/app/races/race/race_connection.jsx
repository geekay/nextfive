import { connect } from 'react-redux'
import RaceContainer from './race_container'
import { fetchCompetitors } from '../races_actions'

const mapStateToProps = ({ races }) => ({
  competitors: races.competitors
})

const mapDispatchToProps = dispatch => ({
  fetchCompetitors: (raceID) => fetchCompetitors(raceID)
})

export default connect(mapStateToProps, mapDispatchToProps)(RaceContainer)