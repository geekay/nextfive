import React from 'react'

const Competitor = ({name, position}) => (
  <div className="competitor">
    <div className="header">
      { name } 
    </div>
    <div className="competitor-position">
      Position: { position }
    </div>
  </div>
)

export default Competitor