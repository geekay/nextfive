import { RECEIVE_RESPONSE_TEXT, receiveError } from './races_actions'
import { history } from '../shared_components/router'
import * as Errors from '../constants/error_codes'
import ErrorMessages from '../constants/error_messages'

const RacesMiddleware = store => next => action => {
  let { type, responseText } = action
  let errorCode = parseInt(responseText)
  let errorMessage = ErrorMessages[errorCode]

  switch(type) {
    case RECEIVE_RESPONSE_TEXT:
      if (errorMessage) {
        store.dispatch(receiveError(errorMessage))
        return next(action)
      }
      if (isNaN(responseText)) {
        store.dispatch(receiveError(responseText))
        return next(action)
      }

      switch(errorCode) {
        default:
          store.dispatch(receiveError("There was a problem. Error code: " + responseText))
          return next(action)
      }
    default: next(action)
  }
}

export default RacesMiddleware
