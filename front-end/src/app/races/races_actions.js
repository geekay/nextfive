import * as API from './races_access'
import store from '../store'
import { history } from '../shared_components/router'
import { getNextFive, getCompetitors } from './races_access'

// Action Types

// Intercepted by Middleware
export const receiveResponseText = responseText => ({
  type: RECEIVE_RESPONSE_TEXT,
  responseText
})

// Emitted by Middleware for Reducer
export const receiveError = error => ({
  type: RECEIVE_ERROR,
  error
})

export const RECEIVE_RACES = "RECEIVE_RACES"
export const RECEIVE_COMPETITORS = "RECEIVE_COMPETITORS"

const receiveRaces = nextFive => ({
  type: RECEIVE_RACES,
  nextFive
})

const receiveCompetitors = competitors => ({
  type: RECEIVE_COMPETITORS,
  competitors
})

export function fetchNextFive() {
  API.getNextFive(
    nextFive => { store.dispatch(receiveRaces(nextFive)) },
    error
  )
}

export function fetchCompetitors(raceID) {
  API.getCompetitors(
    raceID,
    competitors => store.dispatch(receiveCompetitors(competitors)),
    error
  )
}

let error = ({ responseText }) => {
  responseText = responseText ? responseText.replace(/\n/, '') : ''
  store.dispatch(receiveResponseText(responseText))
}