import React from 'react'

const App = ({ children }) => (
  <div>
    <div className='app-content'>
      <div className='app-content-container'>
        {
          children
        }
      </div>
    </div>
  </div>
)

export default App
