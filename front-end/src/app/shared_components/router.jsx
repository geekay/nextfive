// React and Redux
import React from 'react'
import { Provider } from 'react-redux'
import { Switch, Router, Redirect, Route } from 'react-router-dom'
import { createBrowserHistory } from 'history'
import { ConnectedRouter } from 'react-router-redux'

// Components
import App from './app'

import NextFive from '../races/next_five/next_five_connection'
import Race from '../races/race/race_connection'

// History
export const history = createBrowserHistory()

// Added from https://github.com/ReactTraining/react-router/issues/2144#issuecomment-150939358
// To fix pushing page to top when changing routes.
history.listen(location => {
  // Use setTimeout to make sure this runs after React Router's own listener
  setTimeout(() => {
    // Keep default behavior of restoring scroll position when user:
    // - clicked back button
    // - clicked on a link that programmatically calls `history.goBack()`
    // - manually changed the URL in the address bar (here we might want
    // to scroll to top, but we can't differentiate it from the others)
    if (location.action === 'POP') {
      return;
    }
    // In all other cases, check fragment/scroll to top
    var hash = window.location.hash;
    if (hash) {
      var element = document.querySelector(hash);
      if (element) {
        element.scrollIntoView({block: 'start', behavior: 'smooth'});
      }
    } else {
     window.scrollTo(0, 0);
    }
  });
});

// Router
const Index = ({ store }) => (
  <Provider store={ store }>
    <ConnectedRouter history={ history }>
      <App>
        <Route exact path='/' component={NextFive} />
        <Route path='/races/:raceid/competitors' component={Race} />
      </App>
    </ConnectedRouter>
  </Provider>
)

export default Index
