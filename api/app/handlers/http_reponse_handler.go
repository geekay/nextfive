package handlers

import (
	"encoding/json"
	"net/http"
)

//APIResponse structure
type APIResponse struct {
	Success bool        `json:"success"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

//HTTPResponse handles posting a response from data operations
func HTTPResponse(api APIResponse, w http.ResponseWriter) {

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	if api.Success == false {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(api)
		return
	}

	//Returns 200 if no error occurs so no need to set header
	if err := json.NewEncoder(w).Encode(api); err == nil {
		return
	}

	//Finally - somthing bad has happened
	api.Success = false
	api.Message = "Bad Request"
	w.WriteHeader(http.StatusNoContent)
	json.NewEncoder(w).Encode(api)
}
