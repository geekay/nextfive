package router

import (
	"github.com/gorilla/mux"
	"gitlab.com/geekay/nextfive/api/app/common/handlers"
	"gitlab.com/geekay/nextfive/api/app/races"
)

// addRoutes is the application routes
func addAppRoutes(r *mux.Router) {
	r.Handle("/{raceid}/competitors", handlers.RequestHandler(races.GetCompetitorsByRaceID)).Methods("GET")
}

// addBaseRoutes is the base of all our routes
func addBaseRoutes(r *mux.Router) {
	//Index Path
	r.Handle("/", handlers.RequestHandler(races.GetNextFive)).Methods("GET")
}
