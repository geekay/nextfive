package data

import (
	"fmt"
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"strconv"
)

func OpenDB() *gorm.DB {

	dbHost := os.Getenv("DB_HOST")
	dbName := os.Getenv("DB_NAME")
	dbUser := os.Getenv("DB_USER")
	dbPass := os.Getenv("DB_PASS")
	dbPort := os.Getenv("DB_PORT")
	dbSSL := os.Getenv("DB_SSL")

	connectionString := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=%s", dbHost, dbPort, dbUser, dbPass, dbName, dbSSL)

	db, err := gorm.Open("postgres", connectionString)
	if err != nil {
		panic("failed to connect database " + connectionString)
	}
	logMode, err := strconv.ParseBool(os.Getenv("DB_LOGGING_ENABLED"))
	if err != nil {
		fmt.Printf("failed to read log enabled flag from env")
	}
	db.LogMode(logMode)
	return db
}
