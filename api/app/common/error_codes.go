package common

const (
	ParameterIDNotValid            = 000001
	ErrorWhenRetrievingRaces       = 000002
	ErrorWhenRetrievingCompetitors = 000003
	AllGood                        = 999999
)
