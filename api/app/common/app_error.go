package common

type AppError struct {
	Error      error
	AppCode    int
	StatusCode int
}
