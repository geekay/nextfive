package common

// BuildNumber records the continuous integration build number
var BuildNumber = "undefined"

// Version records the DVCS commit ref
var Version = "undefined"
