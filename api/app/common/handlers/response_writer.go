package handlers

import (
	"encoding/json"
	"net/http"
	"strings"
)

func writeResponse(w http.ResponseWriter, r *http.Request, result interface{}) {
	if strings.EqualFold(r.Method, "POST") {
		write(w, http.StatusCreated, result)
	} else {
		write(w, http.StatusOK, result)
	}
}

func write(w http.ResponseWriter, statusCode int, result interface{}) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(statusCode)
	json.NewEncoder(w).Encode(result)
}
