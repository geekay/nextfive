package handlers

import (
	"fmt"
	"log"
	"log/syslog"
	"net/http"
	"os"
	"strings"
)

func handleError(w http.ResponseWriter, inputErr error, message string, statusCode int) {
	LogError(inputErr)
	http.Error(w, message, statusCode)
}

func LogError(inputErr error) {
	var env string = os.Getenv("ENV")
	if strings.ToLower(env) == "dev" {
		fmt.Println(inputErr)
	} else {
		logRemotely(inputErr)
	}
}

func logRemotely(inputErr error) {
	address := os.Getenv("REMOTE_LOG_URL")
	w, networkErr := syslog.Dial("udp", address, syslog.LOG_EMERG|syslog.LOG_KERN, "BondCover")
	if networkErr != nil {
		log.Fatal("failed to dial syslog")
	}

	if inputErr != nil {
		w.Err(inputErr.Error())
	}
}

func infoRemotely(info string) {
	address := os.Getenv("REMOTE_LOG_URL")
	w, networkErr := syslog.Dial("udp", address, syslog.LOG_EMERG|syslog.LOG_KERN, "BondCover")
	if networkErr != nil {
		log.Fatal("failed to dial syslog")
	}

	w.Info(info)

}

func LogInfo(info string) {
	var env string = os.Getenv("ENV")
	if strings.ToLower(env) == "dev" {
		fmt.Println(info)
	} else {
		infoRemotely(info)
	}
}
