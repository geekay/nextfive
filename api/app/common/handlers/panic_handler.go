package handlers

import (
	"fmt"
	"net/http"
	"runtime"
)

// PanicHandler is a Negroni middleware that recovers from any panics and writes a 500 if there was one.
type PanicHandler struct {
	PrintStack       bool
	ErrorHandlerFunc func(interface{})
	StackAll         bool
	StackSize        int
}

// NewRecovery returns a new instance of PanicHandler
func NewPanicHandler() *PanicHandler {
	return &PanicHandler{
		PrintStack: true,
		StackAll:   false,
		StackSize:  1024 * 8,
	}
}

func (fn *PanicHandler) ServeHTTP(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	defer func() {
		if err := recover(); err != nil {
			stack := make([]byte, fn.StackSize)
			stack = stack[:runtime.Stack(stack, fn.StackAll)]

			if fn.PrintStack {
				handleError(w, fmt.Errorf("%v", err), "server error", 500)
				handleError(w, fmt.Errorf("%v", string(stack)), "server error", 500)
			}

			if fn.ErrorHandlerFunc != nil {
				func() {
					defer func() {
						if err := recover(); err != nil {
							handleError(w, fmt.Errorf("%v", err), "server error", 500)
							handleError(w, fmt.Errorf("%v", string(stack)), "server error", 500)
						}
					}()
					fn.ErrorHandlerFunc(err)
				}()
			}
		}
	}()
	next(w, r)
}
