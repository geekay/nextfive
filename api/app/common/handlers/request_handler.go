package handlers

import (
	"net/http"
	"strconv"

	"gitlab.com/geekay/nextfive/api/app/common"
)

type RequestHandler func(w http.ResponseWriter, r *http.Request) (interface{}, *common.AppError)

func (fn RequestHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	result, appErr := fn(w, r)
	if appErr != nil {
		handleError(w, appErr.Error, strconv.Itoa(appErr.AppCode), appErr.StatusCode)
		return
	}

	writeResponse(w, r, result)
}
