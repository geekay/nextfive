package common

import (
	"fmt"
	"net/http"
)

// HealthCheckHandler responds with 200 for load balancing health checks
func HealthCheckHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	response := fmt.Sprintf("Version: %s, BuildNumber: %s", Version, BuildNumber)
	w.Write([]byte(response))

}
