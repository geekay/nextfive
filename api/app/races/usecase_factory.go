package races

func NewUseCase() InputPort {
	return &RacesUseCase{
		CompetitorRepo: &PgCompetitorRepo{},
		RaceRepo:       &PgRaceRepo{},
	}
}
