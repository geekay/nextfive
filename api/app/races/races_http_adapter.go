package races

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/pborman/uuid"
	"gitlab.com/geekay/nextfive/api/app/common"
)

func GetNextFive(w http.ResponseWriter, r *http.Request) (interface{}, *common.AppError) {
	useCase := NewUseCase()

	nextFive, nextFiveErr := useCase.GetNextFive()
	if nextFiveErr != nil {
		return nil, nextFiveErr
	}

	return nextFive, nil
}

func GetCompetitorsByRaceID(w http.ResponseWriter, r *http.Request) (interface{}, *common.AppError) {

	raceID := uuid.Parse(mux.Vars(r)["raceid"])
	if raceID == nil {
		return nil, &common.AppError{fmt.Errorf("Race id is not valid"), common.ParameterIDNotValid, 400}
	}

	competitors, competitorsErr := NewUseCase().GetCompetitorsByRaceID(raceID)
	if competitorsErr != nil {
		return nil, competitorsErr
	}

	return competitors, nil
}
