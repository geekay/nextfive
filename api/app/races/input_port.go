package races

import (
	"github.com/pborman/uuid"
	"gitlab.com/geekay/nextfive/api/app/common"
)

type InputPort interface {
	GetNextFive() ([]Race, *common.AppError)
	GetCompetitorsByRaceID(raceID uuid.UUID) ([]Competitor, *common.AppError)
}
