package races

type RaceType int

const (
	Thoroughbred = 1
	Greyhounds   = 2
	Harness      = 3
)
