package races

import (
	"github.com/jinzhu/gorm"
	"github.com/pborman/uuid"
)

type Competitor struct {
	gorm.Model     `json:"-"`
	CompetitorID   uuid.UUID `json:"-"`
	Position       int       `json:"position"`
	CompetitorName string    `json:"competitorName"`
	RaceID         uuid.UUID `json:"raceID"`
}

func (Competitor) TableName() string {
	return "competitor"
}
