package races

import (
	"github.com/pborman/uuid"
	"gitlab.com/geekay/nextfive/api/app/data"
)

type PgCompetitorRepo struct {
}

func (repo *PgCompetitorRepo) GetAllByRaceID(raceID uuid.UUID) ([]Competitor, error) {
	db := data.OpenDB()
	defer db.Close()

	competitors := []Competitor{}
	result := db.Debug().Order("position asc").Where(&Competitor{RaceID: raceID}).Find(&competitors)
	if result.RecordNotFound() {
		return nil, nil
	}

	return competitors, result.Error
}
