package races

import (
	"time"

	"gitlab.com/geekay/nextfive/api/app/data"
)

type PgRaceRepo struct {
}

func (repo *PgRaceRepo) GetNextFive(currentTime time.Time) ([]Race, error) {
	db := data.OpenDB()
	defer db.Close()

	races := []Race{}
	result := db.Debug().Order("close_time asc").Limit(5).Where("close_time > ?", currentTime).Find(&races)
	//This is due a GORM flaw, in which a record not found returns an error
	if result.RecordNotFound() {
		return nil, nil
	}

	return races, result.Error
}
