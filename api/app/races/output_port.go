package races

import (
	"time"

	"github.com/pborman/uuid"
)

type RaceRepo interface {
	GetNextFive(currentTime time.Time) ([]Race, error)
}

type CompetitorRepo interface {
	GetAllByRaceID(raceID uuid.UUID) ([]Competitor, error)
}
