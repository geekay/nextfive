package races

import (
	"time"

	"github.com/jinzhu/gorm"
	"github.com/pborman/uuid"
)

type Race struct {
	gorm.Model `json:"-"`
	RaceID     uuid.UUID `json:"raceID"`
	RaceType   int       `json:"raceType"`
	CloseTime  time.Time `json:"closeTime"`
}

func (Race) TableName() string {
	return "race"
}
