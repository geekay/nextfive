package races

import (
	"time"

	"github.com/pborman/uuid"
	"gitlab.com/geekay/nextfive/api/app/common"
)

type RacesUseCase struct {
	CompetitorRepo CompetitorRepo
	RaceRepo       RaceRepo
}

func (uc *RacesUseCase) GetNextFive() ([]Race, *common.AppError) {
	currentTime := time.Now().UTC()

	races, racesErr := uc.RaceRepo.GetNextFive(currentTime)
	if racesErr != nil {
		return nil, &common.AppError{racesErr, common.ErrorWhenRetrievingRaces, 500}
	}

	return races, nil
}

func (uc *RacesUseCase) GetCompetitorsByRaceID(raceID uuid.UUID) ([]Competitor, *common.AppError) {
	competitors, competitorsErr := uc.CompetitorRepo.GetAllByRaceID(raceID)
	if competitorsErr != nil {
		return nil, &common.AppError{competitorsErr, common.ErrorWhenRetrievingCompetitors, 500}
	}

	return competitors, nil
}
