package controllers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"reflect"
	"regexp"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/geekay/nextfive/api/app/data"
	"gitlab.com/geekay/nextfive/api/app/handlers"
)

//GetRecord tries to find a record in the data source based on the GORM model supplied
func GetRecord(w http.ResponseWriter, r *http.Request, m interface{}) {

	activeModel := reflect.New(reflect.TypeOf(m)).Interface()
	var response handlers.APIResponse
	vars := mux.Vars(r)

	if _, err := strconv.Atoi(vars["id"]); err != nil {
		response.Success = false
		response.Message = "Invalid ID"
		handlers.HTTPResponse(response, w)
		return
	}

	db := data.OpenDB()
	defer db.Close()

	if database := db.Debug().Find(activeModel, vars["id"]); database.RecordNotFound() == true {
		response.Success = false
		response.Message = "Record Not Found"
		handlers.HTTPResponse(response, w)
		return
	}

	response.Success = true
	response.Message = "Record Found"
	response.Data = activeModel
	handlers.HTTPResponse(response, w)
}

//UpdateRecord is the generic hander for creating or updaing a GORM record
func UpdateRecord(w http.ResponseWriter, r *http.Request, m interface{}) {

	var response handlers.APIResponse
	vars := mux.Vars(r)

	activeModel := reflect.New(reflect.TypeOf(m)).Interface()
	decodedData := activeModel

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&decodedData)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		panic(err)
	}
	defer r.Body.Close()

	db := data.OpenDB()
	defer db.Close()

	//Have ID so update the record
	if vars["id"] != "" {
		db.Debug().Model(activeModel).Updates(decodedData)
		response.Success = true
		response.Message = "Record Updated"
		response.Data = fmt.Sprintf(vars["id"])
		handlers.HTTPResponse(response, w)
		return
	}

	//No ID so create a new record
	db.Debug().Create(decodedData)
	response.Success = true
	response.Message = "Record Created"

	//TODO Seems to be the only way I can get the new record ID back
	re := regexp.MustCompile("(\\d+\\s)")
	response.Data = re.FindStringSubmatch(fmt.Sprintf("%v", decodedData))[0]
	handlers.HTTPResponse(response, w)

}

//DeleteRecord is the generic hander for deleting a GORM record
func DeleteRecord(w http.ResponseWriter, r *http.Request, m interface{}) {
	activeModel := reflect.New(reflect.TypeOf(m)).Interface()

	var response handlers.APIResponse
	vars := mux.Vars(r)

	if _, err := strconv.Atoi(vars["id"]); err != nil {
		response.Success = false
		response.Message = "Invalid ID"
		handlers.HTTPResponse(response, w)
		return
	}

	db := data.OpenDB()
	defer db.Close()
	db.Delete(activeModel, vars["id"])
	response.Success = true
	response.Message = "Record Deleted"
	response.Data = fmt.Sprint(vars["id"])
	handlers.HTTPResponse(response, w)
}
