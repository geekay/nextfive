package controllers

import (
	"fmt"
	"net/http"
)

//IndexHandler returns the index (default)
func IndexHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	fmt.Fprintln(w, "NextFive API")
}
