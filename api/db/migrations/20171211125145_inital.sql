
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied

CREATE TABLE IF NOT EXISTS race (
    id integer NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    deleted_at timestamp with time zone,
    race_id bytea NOT NULL UNIQUE,
    close_time timestamp with time zone,
    race_type int,
    CONSTRAINT race_pkey PRIMARY KEY (id)
);

CREATE SEQUENCE race_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE race_id_seq OWNED BY race.id;

ALTER TABLE ONLY race ALTER COLUMN id SET DEFAULT nextval('race_id_seq'::regclass);

CREATE INDEX idx_race_deleted_at ON race USING btree (deleted_at);

--------------------------------------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS competitor (
    id integer NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    deleted_at timestamp with time zone,
    competitor_id bytea NOT NULL UNIQUE,
    competitor_name text,
    race_id bytea NOT NULL,
    position int,
    CONSTRAINT competitor_pkey PRIMARY KEY (id),
    CONSTRAINT competitor_race_id_fkey FOREIGN KEY (race_id) REFERENCES race(race_id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE SEQUENCE competitor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE competitor_id_seq OWNED BY competitor.id;

ALTER TABLE ONLY competitor ALTER COLUMN id SET DEFAULT nextval('competitor_id_seq'::regclass);

CREATE INDEX idx_competitor_deleted_at ON competitor USING btree (deleted_at);

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back

DROP TABLE IF EXISTS race CASCADE;
DROP TABLE IF EXISTS competitor CASCADE;

