package main

import (
	"gitlab.com/geekay/nextfive/api/cmd"
)

func main() {
	cmd.RootCmd.Execute()
}
