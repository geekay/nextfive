package cmd

import (
	"log"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/joho/godotenv"
	"github.com/spf13/cobra"
	"gitlab.com/geekay/nextfive/api/app/common"
	"gitlab.com/geekay/nextfive/api/app/data"
	"gitlab.com/geekay/nextfive/api/app/router"
)

//Database gobal var
var Database *gorm.DB

// BuildNumber records the continuous integration build number
var BuildNumber = "undefined"

// Version records the DVCS commit ref
var Version = "undefined"

var serverCmd = &cobra.Command{
	Use:   "server",
	Short: "Runs http server",
}

func init() {
	serverCmd.Run = server
}

func server(cmd *cobra.Command, args []string) {
	loadEnv()
	router.NewRouter()
	log.Printf("Server started")

	Database = data.OpenDB()

	select {}
}

func loadEnv() {
	common.Version, common.BuildNumber = Version, BuildNumber
	log.Printf("Start server ...")
	//Load .env file (not checked into repo for security)
	err := godotenv.Load("config/.env")
	if err != nil {
		log.Fatal("Error loading .env file")
	}
}
