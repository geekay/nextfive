package cmd

import (
	"fmt"
	"log"
	"math/rand"
	"os"
	"time"

	"github.com/jinzhu/gorm"
	"github.com/joho/godotenv"
	"github.com/pborman/uuid"

	"github.com/spf13/cobra"
	"gitlab.com/geekay/nextfive/api/app/data"
	"gitlab.com/geekay/nextfive/api/app/races"
)

// TestSeedParams structure to hold the task parameters
type TestSeedParams struct {
	Races    int
	Database *gorm.DB
}

var testSeedCmd = &cobra.Command{
	Use:   "testseed",
	Short: "Insert test seed data in database",
	Long:  "Insert test seed data in database",
}

func init() {
	params := TestSeedParams{}

	testSeedCmd.Run = params.testSeed

	testSeedCmd.Flags().IntVarP(&params.Races, "races", "r", 0, "Races")

}

//testSeed
func (params *TestSeedParams) testSeed(cmd *cobra.Command, args []string) {
	err := godotenv.Load("config/.env")
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	if os.Getenv("ENV") != "dev" {
		log.Fatal("Test seed is working only in dev env")
	}

	rand.Seed(time.Now().UTC().UnixNano())

	database := data.OpenDB()
	defer database.Close()

	params.Database = database

	if params.Races == 0 {
		fmt.Println("Please specify the number of races.")
		return
	}

	//Create races
	for i := 1; i <= params.Races; i++ {
		race := params.addRace()
		// Add the competitors
		for j := 1; j <= params.randInt(4, 8); j++ {
			params.addCompetitor(j, race)
		}
	}

	fmt.Println("Races and competitors were added")

	return
}

//Random int generator
func (params *TestSeedParams) randInt(min int, max int) int {
	return min + rand.Intn(max-min)
}

//Randomly add 1-5 minutes to current time
func (params *TestSeedParams) randTime() time.Time {
	randomInt := params.randInt(1, 6)
	randomTime := time.Now().Add(time.Duration(randomInt) * time.Minute).UTC()

	return randomTime
}

func (params *TestSeedParams) addRace() *races.Race {
	raceType := params.randInt(1, 4)
	randTime := params.randTime()

	race := &races.Race{
		RaceID:    uuid.NewRandom(),
		RaceType:  raceType,
		CloseTime: randTime,
	}

	params.Database.Create(race)

	return race
}

func (params *TestSeedParams) addCompetitor(position int, race *races.Race) *races.Competitor {
	competitor := &races.Competitor{
		CompetitorID:   uuid.NewRandom(),
		Position:       position,
		CompetitorName: fmt.Sprintf("Competitor %d%d", race.ID, position),
		RaceID:         race.RaceID,
	}

	params.Database.Create(competitor)

	return competitor
}
