package cmd

import "github.com/spf13/cobra"

// RootCmd stores cobra commands
var RootCmd = &cobra.Command{}

func init() {
	RootCmd.AddCommand(serverCmd)
	RootCmd.AddCommand(testSeedCmd)
}
